#!/usr/bin/python3
# -*- coding: utf-8 -*-

from typing import Dict, List
from aiohttp import web
from json import dumps
from collections import defaultdict

topics = {
    'новости': ["деревья на садовом кольце", "добрый автобус", "выставка it-технологий"],
    'кухня': ["рецепт борща", "яблочный пирог", "тайская кухня", "китайская кухня"],
    'товары': ["дети капитана гранта", "зимние шины", "тайская кухня"],
}

# transformed_topics
t_topics = defaultdict(lambda: defaultdict(list))


def transform_topics(t: Dict[str, List[str]]) -> None:
    """
    Transforms data to more flexible format

    Args
    ----
    t: Dict[str, List[str]]
        key is a topic, value is an array with articles for this topic
    """
    t_topics.clear()
    for topic_name, topic_articles in t.items():
        for ta_idx, article in enumerate(topic_articles):
            for art_item in article.split():
                t_topics[art_item][topic_name].append(topics[topic_name][ta_idx])


def search_user_str(user_str) -> Dict[str, List[str]]:
    """
    Method to search string from user request in data structure

    Args
    ----
    user_str: str
        User input.

    Return
    ------
    matches: Dict[str, List[str]]
        key is topic, value is a list containing articles for this topic
    """
    looks_like_match = {}
    matches = defaultdict(list)
    user_set = set(user_str.split())
    for str_part in map(lambda x: x.lower(), user_str.split()):
        topics_part = t_topics[str_part]
        if not topics_part:
            continue
        for topic, article in topics_part.items():
            looks_like_match[topic] = article
    for topic, articles in looks_like_match.items():
        for article in articles:
            splitted_article = set(article.split())
            if len(splitted_article & user_set) < len(splitted_article):
                continue
            matches[topic].append(article)
    return matches


async def handle(request):
    request = await request.json()
    user_str = request['user_str']
    response = search_user_str(user_str)
    return web.Response(text=dumps(response, ensure_ascii=False))


if __name__ == "__main__":
    transform_topics(topics)
    app = web.Application()
    app.add_routes([web.post('/', handle)])
    web.run_app(app)

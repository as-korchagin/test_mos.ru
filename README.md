Simple async web-server with search inside.

How to use:

    1. Start server. Starts at 0.0.0.0:8080
    2. Send POST requerst with JSON data inside. JSON should look like '{"user_str": <user string>}'
        
        
Request example:
wget --quiet \
     --method POST \
     --header 'content-type: application/json' \
     --header 'cache-control: no-cache' \
     --body-data '{\n	"user_str": "где купить зимние шины"\n}' \
     --output-document \
     - http://127.0.0.1:8080/

Response example:
    {"товары": ["зимние шины"]}